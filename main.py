'''

Main python function to seperate the documents!

use double quotes in main file
using python3

'''

import filter_char
import filter_word
import numpy as np
import math

def push_to_dict(dictionary, word):
    if (len(word) > 0):
        try:
            dictionary[word] += 1
        except:
            dictionary[word] = 1

def parse(doc, dictionary, grammar):
    word = ''
    while 1:
        c = doc.read(1)
        if not c:
            push_to_dict(dictionary, word)
            word = ''
            break
        else:
            #print('word', word)
            result = filter_char.filter_char(c)
            if result['decision'] == 0:
                continue
            elif result['decision'] == 1:
                word += result['normalized_char']
            elif result['decision'] == 2:
            	push_to_dict(dictionary, result['replace_word'])
            	word_result = filter_word.filter_word(word, grammar)
                if word_result['decision'] == 0:
                    word = ''
                    continue
                elif word_result['decision'] == 1:
                    push_to_dict(dictionary, word)
                	word = ''
                	continue
            elif result['decision'] == 3:
                word_result = filter_word.filter_word(word, grammar)
                if word_result['decision'] == 0:
                    word = ''
                    continue
                elif word_result['decision'] == 1:
                    push_to_dict(dictionary, word)
                	word = ''
                	continue

def main(path_1, path_2, filter_grammar=['article', 'sub_pronoun', 'obj_pronoun', 'w_question', 'conjuction', 'preposition']):

    doc_1 = open(path_1, 'r')
    doc_2 = open(path_2, 'r')

    dict_1 = {}
    dict_2 = {}

    parse(doc_1, dict_1, filter_grammar)
    parse(doc_2, dict_2, filter_grammar)

    unique_words = list(set(list(dict_1.keys()) + list(dict_2.keys())))

    # print('dict keys', dict_1.keys(),dict_2.keys())

    vec_1 = np.zeros(len(unique_words))
    vec_2 = np.zeros(len(unique_words))

    for i in range(len(unique_words)):
        if unique_words[i] in dict_1.keys():
            vec_1[i] = dict_1[unique_words[i]]
        if unique_words[i] in dict_2.keys():
            vec_2[i] = dict_2[unique_words[i]]

    product = vec_1.dot(vec_2)
    len_1 = math.sqrt(vec_1.dot(vec_1))
    len_2 = math.sqrt(vec_2.dot(vec_2))

    # print(unique_words)
    # print(len_1,len_2,product,vec_1,vec_2)

    angle = math.acos(product / (len_1 * len_2)) * (180 / math.pi)
    return angle

print(main('test/input1.txt', 'test/input2.txt'))


