"""

Function decide weather a character is required or not.

"""

"""
return_codes:
-1 -> not satisfied
0 -> satisfied and some letter / number
1 -> %
2 -> $
3 -> ' '
4 -> \n

"""

"""
desicion:
0 -> skip.
1 -> append and read next.
2 -> dont append, create a new word, if the previous recorded word is not empty, use it
3 -> is space

"""

char_regexp = '[A-Za-z0-9$%]' # Not using, just for refrence.

def filter_char(c):
	if (c >= 'A' and c <= 'Z') or (c >= 'a' and c <='z'):
		return {
			'return_code': 0,
			'normalized_char': c.lower(),
			'decision': 1,
			'meta': 'alpha'
		}

	if (c >= '0' and c <= '9'):
		return {
			'return_code': 0,
			'normalized_char': c,
			'decision': 1,
			'meta': 'numero'
		}

	if c == '%':
		return {
			'return_code': 1,
			'replace_word': 'percent',
			'decision': 2,
			'meta': 'special'
		}

	if c == '$':
		return {
			'return_code': 2,
			'replace_word': 'dollar',
			'decision': 2,
			'meta': 'special'
		}

	if c == ' ':
		return {
			'return_code': 3,
			'normalized_char': c,
			'decision': 3,
			'meta': 'space'
		}

	if c == '\n':
		return {
			'return_code': 4,
			'normalized_char': c,
			'decision': 3,
			'meta': 'new-line'
		}

	return {
		'return_code': -1,
		'decision': 0
	}
