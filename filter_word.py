"""

Function to decide weather a word in required or not.

"""

filter_wordset = {
	'article': ['a', 'the', 'an'],
	'sub_pronoun': ['i', 'you', 'he', 'she', 'it', 'they', 'we'],
	'obj_pronoun': ['me', 'you', 'him', 'her', 'it', 'us', 'them'],
	'w_question': ['why', 'when', 'what', 'which', 'where'],
	'conjuction': ['for', 'and', 'nor', 'but', 'or', 'yet', 'so'],
	'preposition': ['on', 'in', 'under', 'below', 'above']
}


"""
desicion:
0 -> skip.
1 -> add to dictionary or increment if exists.

"""

"""
word_type:
unique, pronoun, article etc.

"""

def filter_word(word, filter_grammar):
	if len(word) == 0:
		return {
			'decision': 0,
			'word_type': 'null'
		}
	for g_set in filter_wordset.keys():
		if word in filter_wordset[g_set]:
			if g_set in filter_grammar:
				return {
					'decision': 0,
					'word_type': g_set
				}
			else:
				return {
					'decision': 1,
					'word_type': g_set
				}

	return {
		'decision': 1,
		'word_type': 'unique'
	}