# README #

Simple set of functions to find the distance between 2 documents, written in Python.  

This is a part of the Paradigms of Programing Course. (Mini Project 1)  

**Team:**  
Adithya Kumar  
Varshith Polu  
Sai Nishanth Vaka  
Kasi Reddy Durga Prasad  